Beispiel für eine einfache HTML-Seite mit GitLab Pages.

Mehr über GitLab Pages erfährst du unter https://pages.gitlab.io und in der offiziellen
Dokumentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Inhaltsverzeichnis**  *erstellt mit [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab Benutzer- oder Gruppenseiten](#gitlab-user-or-group-pages)
- [Hast du dieses Projekt geforkt?](#did-you-fork-this-project)
- [Fehlersuche](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

Die statischen Seiten dieses Projekts werden von [GitLab CI][ci] erstellt, indem die Schritte
die in [`.gitlab-ci.yml`](.gitlab-ci.yml) definiert sind:

```
image: busybox

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
    expire_in: 1 day
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

Das obige Beispiel erwartet, dass alle deine HTML-Dateien im Verzeichnis `public/` abgelegt werden.

## GitLab Benutzer- oder Gruppenseiten

Um dieses Projekt als Benutzer- oder Gruppenseite zu verwenden, brauchst du noch einen
Schritt: Benenne dein Projekt einfach in "namespace.gitlab.io" um, wobei "namespace" für
dein `Benutzername` oder `Gruppenname` ist. Das kannst du tun, indem du zu deinem
Projekt **Einstellungen**.

Lies mehr über [user/group Pages][userpages] und [project Pages][projpages].

## Hast du dieses Projekt geforkt?

Wenn du dieses Projekt für deine eigenen Zwecke geforkt hast, gehe bitte zu deinem Projekt
**Einstellungen** und entferne die Forking-Beziehung, was nicht notwendig ist
es sei denn, du möchtest wieder zum Upstream-Projekt beitragen.

## Fehlersuche

1. CSS fehlt! Das bedeutet, dass du die CSS-URL in deinen HTML-Dateien falsch eingestellt hast.
   HTML-Dateien gesetzt hast. Sieh dir die [index.html] als Beispiel an.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
